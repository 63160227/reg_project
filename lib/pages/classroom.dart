import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

class ClassroomPage extends StatelessWidget {
  const ClassroomPage({super.key});

  Widget build(BuildContext context) {
    double rowPadding = 24.0;
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(color: Colors.blue),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              // Align however you like (i.e .centerRight, centerLeft)
              child: Text(
                "ตารางเรียน ภาคเรียนที่ 1/2566",
                style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.w900,
                  color: Colors.white,
                ),
              ),
            ),
            const SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.all(rowPadding),
                  child: Column(
                    children: [
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.0),
                        child: Image(
                          image: AssetImage("assets/images/table.jpg"),
                          width: 700,
                          // color: Colors.indigo.shade800,
                        ),
                      ),
                      const SizedBox(
                        height: 50.0,
                      ),
                      Text(
                        "ตารางเรียน ภาคเรียนที่ 2/2566",
                        style: TextStyle(
                            fontSize: 40,
                            fontWeight: FontWeight.w900,
                            color: Colors.white),
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.0),
                        child: Image(
                          image: AssetImage("assets/images/class.jpg"),
                          width: 700,
                          // color: Colors.indigo.shade800,
                        ),
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
