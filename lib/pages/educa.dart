import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

class EducationPage extends StatelessWidget {
  const EducationPage({super.key});

  Widget build(BuildContext context) {
    double rowPadding = 24.0;
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(color: Colors.blue),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              // Align however you like (i.e .centerRight, centerLeft)
              child: Text(
                "ผลการศึกษา",
                style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.w900,
                  color: Colors.white,
                ),
              ),
            ),
            const SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.all(rowPadding),
                  child: Column(
                    children: [
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.0),
                        child: Image(
                          image: AssetImage("assets/images/grad.jpg"),
                          width: 400,
                          // color: Colors.indigo.shade800,
                        ),
                      ),
                      const SizedBox(
                        height: 5.0,
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.0),
                        child: Image(
                          image: AssetImage("assets/images/grad.jpg"),
                          width: 400,
                          // color: Colors.indigo.shade800,
                        ),
                      ),
                      const SizedBox(
                        height: 5.0,
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.0),
                        child: Image(
                          image: AssetImage("assets/images/grad.jpg"),
                          width: 400,
                          // color: Colors.indigo.shade800,
                        ),
                      ),
                      const SizedBox(
                        height: 5.0,
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.0),
                        child: Image(
                          image: AssetImage("assets/images/grad.jpg"),
                          width: 400,
                          // color: Colors.indigo.shade800,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
