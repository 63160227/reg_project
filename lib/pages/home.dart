import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  Widget build(BuildContext context) {
    double rowPadding = 24.0;
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/student.jpg"),
            fit: BoxFit.cover,

          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
        Align(
        alignment: Alignment.topCenter, // Align however you like (i.e .centerRight, centerLeft)
          child: Text(
                "BUU Adminssion66 TCAS",
                style: TextStyle(
                  fontSize: 60,
                  fontWeight: FontWeight.w900,
                  color: Colors.blue.shade900,
                ),
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
            const Text(
              "คณะและหลักสูตรมหาวิทยาลัยบูรพา",
              style: TextStyle(
                  fontSize: 35,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            const Text(
              "มุ่งเน้นบัณฑิตศึกษา วิจัยขั้นสูง พัฒนาวิชาการ บริการวิชาพื้นฐาน บริหารโครงการพิเศษ ประสานความร่วมมือกับนานาชาติ",
              style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w500,
                  color: Colors.white),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.all(rowPadding),
                  child: Column(
                    children: [
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.0),
                        child: Icon(
                          Icons.person, size: 70, color: Colors.white,
                          // color: Colors.indigo.shade800,
                        ),
                      ),
                      Column(
                        children: [
                          Text(
                            "25",
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.w500,
                                color: Colors.white),
                          ),
                          Text(
                            "คณะ",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Colors.white),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(rowPadding),
                  child: Column(
                    children: [
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.0),
                        child: Icon(
                          Icons.blinds, size: 70, color: Colors.white,
                          // color: Colors.indigo.shade800,
                        ),
                      ),
                      Column(
                        children: [
                          Text(
                            "4",
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.w500,
                                color: Colors.white),
                          ),

                          Text(
                            "มหาวิทยาลัย",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Colors.white),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(rowPadding),
                  child: Column(
                    children: [
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.0),
                        child: Icon(
                          Icons.book_rounded, size: 70, color: Colors.white,
                          // color: Colors.indigo.shade800,
                        ),
                      ),
                      Column(
                        children: [
                          Text(
                            "321",
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.w500,
                                color: Colors.white),
                          ),
                          Text(
                            "หลักสูตร",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Colors.white),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter, // Align however you like (i.e .centerRight, centerLeft)
              child: Text(
                "ดูรายละเอียดเพิ่มเติมได้ที่ https://buurapha.co.th",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w300,
                  color: Colors.white,
                ),
              ),
            ),
          ],

        ),

      ),
    );
  }
}
